# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Product(models.Model):
        code = models.CharField(max_length=20, verbose_name='条形码')
        name = models.CharField(max_length=100, verbose_name='产品名字')
	size = models.FloatField(verbose_name='尺寸')
        date = models.DateTimeField('日期')

	class Meta:
		verbose_name = "产品"
		verbose_name_plural = "产品"

	def was_published_recently(self):
                now = timezone.now()
                if now - datetime.timedelta(days=1) <= self.date <= now:
			return unicode("Yes")
		else:
			return unicode("No")
	was_published_recently.short_description = "是否最近更新"

        def __str__(self):
                return self.name

	def __unicode__(self):
		return unicode(self.name)


class Provider(models.Model):
        name = models.CharField(max_length=100, verbose_name="供应商名字")
        product = models.ForeignKey(Product)
        number = models.IntegerField(default=0, verbose_name="数目")

	class Meta:
		verbose_name = "供应商"
		verbose_name_plural = "供应商"

        def __str__(self):
		return self.name

	def __unicode__(self):
		return unicode(self.name)
