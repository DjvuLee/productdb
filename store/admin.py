from django.contrib import admin
from .models import Product, Provider

class ProviderInline(admin.StackedInline):
    model = Provider
    extra = 1

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Basic Info',  {'fields': ['code', 'name']}),
        ('Date Info', {'fields': ['date']}),
        ('Extra Info', {'fields': ['size']}),
    ]
    inlines = [ProviderInline]
    list_display = ('code', 'name', 'size', 'date', 'was_published_recently')
    list_filter = ['code', 'date']
    search_fields = ['code']


admin.site.register(Product, ProductAdmin)
admin.site.register(Provider)
