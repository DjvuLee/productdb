# -*- coding: utf-8 -*-

from django.apps import AppConfig
import sys

reload(sys)
sys.setdefaultencoding("utf-8")
class YourAppConfig(AppConfig):
    name = "store"
    verbose_name = '存储信息'
